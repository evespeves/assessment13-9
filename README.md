
1	Tanya Sanatani nyasanatani@gmail.com   UK 13/09/2019
2   Eva Aherne eva.aherne2@gmail.com UK 13/09/2019


docker compose commands
-docker-compose down --rmi all
-sudo rm -rf ./mydb
-docker-compose up


Who did what:
Eva: Worked on the MySQL docker container and the docker-compose.yaml file
Tanya: Worked on the PHP-Apache container and the docker-compose.yaml file

Brief Explanation of what the application does 
1. Web server(php) connects with a sql database to provide results of users who live in a specific location.
2. The application is containerized using docker for easy deployment and easy configuration. Provides the ability to scale the application easily in the future.
